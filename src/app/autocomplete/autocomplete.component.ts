import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { CountryService } from '../service/country.service';
import { map, startWith } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {

  name = 'Angular';
  formGroup: FormGroup;
  countries$: Observable<Country[]>;
  cities$: Observable<City[]>;

  cities:any;

  constructor(private country: CountryService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getCountries();
    this.form = this.formBuilder.group({
      country: []
    });
    this.filteredCountries = this.form.get('country').valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.name),
      map(name => name ? this._filter(name) : this.countryList.slice())
    );
  }

  selectCountry(selected: MatAutocompleteSelectedEvent) {
    this.formGroup.controls.city.enable();
    let relatedCities: City[] = this.cities.filter(city => city.country == selected.option.value);

    this.cities$ = this.formGroup.controls.city.valueChanges.pipe(
      startWith(null),
      map(
        text => text ?
          relatedCities.filter(
            city =>
              city.name.toLowerCase().includes(text.toLowerCase())
          ) :
          relatedCities
      )
    );
  }

  countryList: Country[] = [];
  filteredCountries: Observable<Country[]>;
  form: FormGroup;

  displayedColumns: string[] = ['name', 'weight', 'symbol', 'position'];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  data: PeriodicElement[] = ELEMENT_DATA;

  addColumn() {
    const randomColumn = Math.floor(Math.random() * this.displayedColumns.length);
    this.columnsToDisplay.push(this.displayedColumns[randomColumn]);
  }

  removeColumn() {
    if (this.columnsToDisplay.length) {
      this.columnsToDisplay.pop();
    }
  }

  shuffle() {
    let currentIndex = this.columnsToDisplay.length;
    while (0 !== currentIndex) {
      let randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // Swap
      let temp = this.columnsToDisplay[currentIndex];
      this.columnsToDisplay[currentIndex] = this.columnsToDisplay[randomIndex];
      this.columnsToDisplay[randomIndex] = temp;
    }
  }


  stateInfo: any[] = [];
  countryInfo: any[] = [];
  cityInfo: any[] = [];



  getCountries() {
    this.country.allCountries().
      subscribe(
        data2 => {
          this.countryInfo = data2.Countries;
          console.log('Data:', this.countryInfo);
        },
        err => console.log(err),
        () => console.log('complete')
      )
  }
  private _filter(value: string): Country[] {
    const filterValue = value.toLowerCase();
    return this.countryInfo.filter(option => option.CountryName.toLowerCase().indexOf(filterValue) === 0);
  }

  displayFn(country: Country): string {
    return country && country.name ? country.name : '';
  }

  onChangeCountry(countryValue) {
    console.log('onSelectionChange countryValue', countryValue);
    console.log('onSelectionChange option', countryValue.option);
    console.log('onSelectionChange called', countryValue.option.value);
    console.log(this.countryInfo[countryValue]);
    this.stateInfo = this.countryInfo[countryValue].value;
    console.log(this.stateInfo);
    this.cityInfo = this.stateInfo[0].Cities;
    console.log(this.cityInfo);
  }

  onChangeState(stateValue) {
    this.cityInfo = this.stateInfo[stateValue].Cities;
    //console.log(this.cityInfo);
  }
}


export interface Country {
  id: number,
  name: string
}

export class Country {
  name: string;
  id: number;
}

export class City {
  name: string;
  country: string;
}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];